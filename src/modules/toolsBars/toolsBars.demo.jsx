import React, { Component } from 'react'
import ToolsBars from './toolsBars'

class ToolsBarsDemo extends Component {
    render() {
        const dataSource = [
            {
                icon: '+',
                effectName: 'click',
                isTooltip: true,
                eventName: 'add',
                eventNameTitle: '添加',
                id: '001'
            },
            {
                icon: '-',
                eventName: 'delete',
                effectName: 'dot',
                isTooltip: false,
                eventNameTitle: '删除',
                id: '002'
            },
            {
                icon: '*',
                eventName: 'modify',
                effectName: 'click',
                isTooltip: false,
                eventNameTitle: '更改',
                id: '003'
            },
            {
                icon: '=',
                eventName: 'search',
                effectName: 'cancel',
                isTooltip: true,
                eventNameTitle: '查询',
                id: '004'
            }
        ]
        return (
            <div>
                <ToolsBars dataSource={dataSource} />
            </div>
        )
    }
}
export default ToolsBarsDemo
