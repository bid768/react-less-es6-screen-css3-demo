import React, { Component } from 'react'
import toolsBarsStyle from './toolsBars.less'
import { Tooltip } from 'antd'
import 'antd/lib/tooltip/style/index.less'
class ToolsBars extends Component {
    constructor(props) {
        super(props)
        this.state = {
            current: 0,
            isToggleOn: false,
            dot: false
        }
        this.myli = React.createRef()
    }

    // 点击切换相应背景色
    handleClick(item, index, e) {
        e.persist()
        if (item.effectName === 'click') {
            if (this.state.isToggleOn === true) {
                return
            } else {
                this.setState({
                    current: index
                })
            }
        }
        if (item.effectName === 'dot') {
            if (this.state.isToggleOn === true) {
                return
            }
            const _this = this
            this.timer = setTimeout(function() {
                _this.myli.current.className = ''
            }, 300)
            this.myli.current.className = toolsBarsStyle.dot
            this.setState({
                current: index
            })
        }
        if (item.effectName === 'cancel') {
            this.setState(prevState => ({
                current: index,
                isToggleOn: !prevState.isToggleOn
            }))
        }
    }

    // 清空定时器
    componentWillUnmount() {
        clearInterval(this.timer)
    }

    // 显示工具栏具体信息
    showToolsBarsInfo = data => {
        const sty = {
            cursor: 'pointer',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
            height: '80px',
            width: '80px'
        }
        const toolsBarsInfo = data.map((item, index) => {
            if (!item.isTooltip) {
                if (item.effectName === 'dot') {
                    return (
                        <li
                            style={sty}
                            key={index}
                            onClick={this.handleClick.bind(this, item, index)}
                            ref={this.myli}
                        >
                            <div>{item.icon}</div>
                            <div>{item.eventNameTitle}</div>
                        </li>
                    )
                }
                if (item.effectName === 'click') {
                    return (
                        <li
                            className={
                                this.state.current === index
                                    ? toolsBarsStyle.active
                                    : toolsBarsStyle.basicBgc
                            }
                            style={sty}
                            key={index}
                            onClick={this.handleClick.bind(this, item, index)}
                        >
                            <div>{item.icon}</div>
                            <div>{item.eventNameTitle}</div>
                        </li>
                    )
                }
                if (item.effectName === 'cancel') {
                    return (
                        <li
                            className={
                                this.state.current === index
                                    ? this.state.isToggleOn
                                        ? toolsBarsStyle.active
                                        : toolsBarsStyle.basicBgc
                                    : toolsBarsStyle.basicBgc
                            }
                            style={sty}
                            key={index}
                            onClick={this.handleClick.bind(this, item, index)}
                        >
                            <div>{item.icon}</div>
                            <div>{item.eventNameTitle}</div>
                        </li>
                    )
                }
            } else {
                if (item.effectName === 'dot') {
                    return (
                        <Tooltip title={item.eventNameTitle} key={index}>
                            <li
                                className={
                                    this.state.current === index
                                        ? toolsBarsStyle.dot
                                        : ''
                                }
                                style={sty}
                                onClick={this.handleClick.bind(
                                    this,
                                    item,
                                    index
                                )}
                                ref={this.myli}
                            >
                                <div>{item.icon}</div>
                            </li>
                        </Tooltip>
                    )
                }
                if (item.effectName === 'click') {
                    return (
                        <Tooltip title={item.eventNameTitle} key={index}>
                            <li
                                className={
                                    this.state.current === index
                                        ? toolsBarsStyle.active
                                        : toolsBarsStyle.basicBgc
                                }
                                style={sty}
                                onClick={this.handleClick.bind(
                                    this,
                                    item,
                                    index
                                )}
                            >
                                <div>{item.icon}</div>
                            </li>
                        </Tooltip>
                    )
                }
                if (item.effectName === 'cancel') {
                    return (
                        <Tooltip title={item.eventNameTitle} key={index}>
                            <li
                                className={
                                    this.state.current === index
                                        ? this.state.isToggleOn
                                            ? toolsBarsStyle.active
                                            : toolsBarsStyle.basicBgc
                                        : toolsBarsStyle.basicBgc
                                }
                                style={sty}
                                onClick={this.handleClick.bind(
                                    this,
                                    item,
                                    index
                                )}
                            >
                                <div>{item.icon}</div>
                            </li>
                        </Tooltip>
                    )
                }
            }
        })
        return (
            <ul
                style={{
                    position: 'relative',
                    display: 'flex',
                    background: 'green',
                    paddingLeft: '0'
                }}
            >
                {toolsBarsInfo}
            </ul>
        )
    }

    render() {
        const { dataSource } = this.props
        return <div>{this.showToolsBarsInfo(dataSource)}</div>
    }
}

export default ToolsBars
