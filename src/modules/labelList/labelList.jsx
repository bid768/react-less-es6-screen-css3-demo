import React from 'react'
import styles from './LabelList.less'

class LabelList extends React.Component {
    renderLabelList() {
        const data = this.props.data
        const ulDom = (
            <ul className={styles.labelList}>
                {data.labels.map((item, index) => (
                    <li className={styles.labelItem} key={index}>
                        {item}
                    </li>
                ))}
            </ul>
        )
        return ulDom
    }

    render() {
        return (
            <div className={styles.labelBox}>
                <div style={{ textAlign: 'center', color: '#70fcb8' }}>
                    flexBox实现标签自适应，标签个数不一定，标签里面的文字个数不一定。
                </div>
                {this.renderLabelList()}
            </div>
        )
    }
}

// 定义默认值
LabelList.defaultProps = {
    data: {
        labels: ['瑜伽', '游泳', '爬山', '长跑', '读书', '写随笔', '英语法语']
    }
}

export default LabelList
