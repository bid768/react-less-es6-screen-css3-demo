import React, { Component } from 'react'
import styles from './wanzi.less'

export default class Wanzi extends Component {
    render() {
        return (
            <div className={styles.wanziWrapper}>
                <div className={styles.wanzi}>
                    <div className={styles.head} />
                    <div className={styles.hair}>
                        <div className={styles.hairBottom} />
                        <div className={styles.chi + ' ' + styles.chi1} />
                        <div className={styles.chi + ' ' + styles.chi2} />
                        <div className={styles.chi + ' ' + styles.chi3} />
                        <div className={styles.chi + ' ' + styles.chi4} />
                        <div className={styles.chi + ' ' + styles.chi5} />
                        <div className={styles.chi + ' ' + styles.chi6} />
                    </div>
                    <div className={styles.hair1} />
                    <div className={styles.hair2} />
                    <div className={styles.hair3} />
                    <div className={styles.eyes}>
                        <div
                            className={styles.meimao + ' ' + styles.leftMeimao}
                        />
                        <div className={styles.eye + ' ' + styles.leftEye}>
                            <div className={styles.eyeBall} />
                        </div>
                        <div
                            className={styles.meimao + ' ' + styles.rightMeimao}
                        />
                        <div className={styles.eye + ' ' + styles.rightEye}>
                            <div className={styles.eyeBall} />
                        </div>
                    </div>
                    <div className={styles.leftEar} />
                    <div className={styles.rightEar} />
                    <div className={styles.face + ' ' + styles.leftFace} />
                    <div className={styles.face + ' ' + styles.rightFace} />
                    <div className={styles.leftShy}>
                        <div className={styles.shy1} />
                        <div className={styles.shy2} />
                        <div className={styles.shy3} />
                    </div>
                    <div className={styles.rightShy}>
                        <div className={styles.shy1} />
                        <div className={styles.shy2} />
                        <div className={styles.shy3} />
                    </div>
                    <div className={styles.mouth} />
                </div>

                <div className={styles.text}>鼠标放她大脸上</div>
            </div>
        )
    }
}
