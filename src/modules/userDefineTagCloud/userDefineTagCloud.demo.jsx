import React, { Component } from 'react'
import UserDefineTagCloud from './userDefineTagCloud'

class UserDefineTagCloudDemo extends Component {
    render() {
        const dataSource = [
            {
                id: '001',
                value: '天龙八部1相龙龙八部1相龙龙八部1相龙',
                tagScore: 50,
                hitCount: 10
            },
            {
                id: '002',
                value: '天部2',
                tagScore: 80,
                hitCount: 10
            },
            {
                id: '003',
                value: '天龙八部3天龙八部',
                tagScore: 80,
                hitCount: 5
            },
            {
                id: '004',
                value: 'slxkdovbmaodj',
                tagScore: 30,
                hitCount: 8
            },
            {
                id: '005',
                value: '天龙八部5八部',
                tagScore: 30,
                hitCount: 4
            },
            {
                id: '006',
                value: '1234667886',
                tagScore: 80,
                hitCount: 7
            }
        ]
        // '#F1E511', '#EE0F3A', 'green'
        const colors = [
            {
                fontColor: '#F1E511',
                bgcColor: 'rgba(58,56,5,0.20)'
            },
            {
                fontColor: '#EE0F3A',
                bgcColor: 'rgba(97,9,26,0.20)'
            },
            {
                fontColor: 'green',
                bgcColor: ''
            }
        ]
        return (
            <div>
                <UserDefineTagCloud dataSource={dataSource} colors={colors} />
            </div>
        )
    }
}
export default UserDefineTagCloudDemo
