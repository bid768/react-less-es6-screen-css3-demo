import React, { Component } from 'react'
// 引用react-tagcloud标签云组件
import { TagCloud } from 'react-tagcloud'
import styles from './userDefineTagCloud.less'
class UserDefineTagCloud extends Component {
    constructor(props) {
        super(props)
        this.state = {}
        this.originDataSource = JSON.parse(
            JSON.stringify(this.props.dataSource)
        )
        this.colors = JSON.parse(JSON.stringify(this.props.colors))
        this.arr = []
        // 处理props数据,适合TagCloud组件的参数
        this.originDataSource.map(item => {
            if (item.tagScore === 50) {
                if (item.hitCount >= 10) {
                    this.arr.push({
                        value: item.value,
                        count: 20,
                        color: this.colors[0].fontColor
                    })
                } else if (item.hitCount >= 5 && item.hitCount < 10) {
                    this.arr.push({
                        value: item.value,
                        count: 15,
                        color: this.colors[0].fontColor
                    })
                } else {
                    this.arr.push({
                        value: item.value,
                        count: 12,
                        color: this.colors[0].fontColor
                    })
                }
            } else if (item.tagScore > 50) {
                if (item.hitCount >= 10) {
                    this.arr.push({
                        value: item.value,
                        count: 20,
                        color: this.colors[1].fontColor
                    })
                } else if (item.hitCount >= 5 && item.hitCount < 10) {
                    this.arr.push({
                        value: item.value,
                        count: 15,
                        color: this.colors[1].fontColor
                    })
                } else {
                    this.arr.push({
                        value: item.value,
                        count: 12,
                        color: this.colors[1].fontColor
                    })
                }
            } else if (item.tagScore < 50) {
                if (item.hitCount >= 10) {
                    this.arr.push({
                        value: item.value,
                        count: 20,
                        color: this.colors[2].fontColor
                    })
                } else if (item.hitCount >= 5 && item.hitCount < 10) {
                    this.arr.push({
                        value: item.value,
                        count: 15,
                        color: this.colors[2].fontColor
                    })
                } else {
                    this.arr.push({
                        value: item.value,
                        count: 12,
                        color: this.colors[2].fontColor
                    })
                }
            }
        })
    }

    // 显示背景色
    showBgcColor(color) {
        if (color === this.colors[0].fontColor) {
            return this.colors[0].bgcColor
        }
        if (color === this.colors[1].fontColor) {
            return this.colors[1].bgcColor
        }
    }

    // 设置每一个标签的样式
    customRenderer = (tag, size, color) => (
        <span
            key={tag.value}
            style={{
                border: `2px solid ${color}`,
                fontSize: `${size}px`,
                color: color,
                background: this.showBgcColor(color)
            }}
            className={styles.tag_cloud_item}
        >
            {tag.value}
        </span>
    )

    render() {
        return (
            <TagCloud
                minSize={12}
                maxSize={20}
                tags={this.arr}
                renderer={this.customRenderer}
                className={styles.tag_cloud_wrapper}
            />
        )
    }
}

export default UserDefineTagCloud
