export const category = [
    { type: 'message' },
    { type: 'triangle' },
    { type: 'trapezoid' },
    { type: 'pentagon' },
    { type: 'rabbet' },
    { type: 'parallelogram' },
    { type: 'octagon' },
    { type: 'leftArrow' },
    { type: 'rhombus' },
    { type: 'rightArrow' },
    { type: 'hexagon' },
    { type: 'leftPoint' },
    { type: 'cross' },
    { type: 'rightPoint' },
    { type: 'star' },
    { type: 'close' },
    { type: 'complex' }
]
