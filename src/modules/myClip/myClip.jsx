import React, { Component } from 'react'
import { category } from './data'
import styles from './myClip.less'

export default class MyClip extends Component {
    constructor(props) {
        super(props)
        this.state = {
            activeIndex: 0
        }
    }

    componentDidMount() {
        this.timer = setInterval(() => {
            this.changeIndex()
        }, 2000)
    }

    changeIndex() {
        let { activeIndex } = this.state
        if (activeIndex === category.length - 1) {
            activeIndex = 0
        } else {
            activeIndex = activeIndex + 1
        }
        this.setState({ activeIndex })
    }

    componentWillUnmount() {
        this.timer && clearInterval(this.timer)
    }

    render() {
        return (
            <div>
                css裁剪遮罩
                <div className={styles.container}>
                    <div
                        className={
                            styles.content +
                            ' ' +
                            styles[category[this.state.activeIndex].type]
                        }
                    />
                    <span className={styles.title}>
                        {category[this.state.activeIndex].type}
                    </span>
                </div>
                <div className={styles.container}>
                    <div className={styles.animateCom} />
                    <span className={styles.title}>mouse hover me</span>
                </div>
            </div>
        )
    }
}
