import React, { Component } from 'react'
import $ from 'jquery'
import styles from './userDefineProgressBar.less'
class UserDefineProgressBar extends Component {
    constructor(props) {
        super(props)

        this.xTotal = 0
    }

    componentDidMount() {
        const container = document.getElementById('list')
        // 创建文档碎片
        const divFargMent = document.createDocumentFragment()

        // hidden的div
        const y =
            (this.props.progressWidth - this.props.xWidth) /
            (this.props.yWidth + this.props.xWidth)
        // visible的div
        const x = y + 1
        // 总的div
        const totalDiv = x + y
        this.xTotal = x
        // 根据需求渲染相应的空div数量
        for (let i = 0; i < totalDiv; i++) {
            const div = document.createElement('div')
            if (i % 2 === 0) {
                div.style.width = `${this.props.xWidth}px` // visible的div的宽度
                div.style.height = `${this.props.progressHeight}px` // visible的div的高度
            } else {
                div.style.width = `${this.props.yWidth}px` // hidden的div的宽度
                div.style.height = `${this.props.progressHeight}px` // hidden的div的高度
            }
            divFargMent.appendChild(div)
        }
        container.appendChild(divFargMent)
        this.progressBarRender()
    }

    // 根据给定的grade显示相应的进度
    progressBarRender() {
        const { grade } = this.props
        const count = grade > 100 ? 100 : grade
        const len = count ? Math.ceil(count / this.props.num) : 0
        for (let i = 1; i <= len; i++) {
            $('#list div')
                .eq((i - 1) * 2)
                .css('visibility', 'hidden')
        }
    }

    // 分数颜色
    gradeColor(grade) {
        if (grade < 31) {
            return '#089eeb'
        } else if (grade > 31 && grade < 73) {
            return '#d9a20d'
        } else {
            return '#db0909'
        }
    }

    render() {
        const {
            grade,
            progressWidth,
            progressHeight,
            size,
            fontTop
        } = this.props
        const width = `${progressWidth}px` // 进度条宽度
        const height = `${progressHeight}px` // 进度条高度
        const bgcStyle = { width, height } // 背景宽高
        const fontSize = `${size}px` // 字体大小
        const top = `${fontTop}px` // grade 位置
        const marginLeft = `${progressWidth + 10}px` // grade 距左边位置
        const gradeStyle = {
            color: this.gradeColor(grade),
            marginLeft,
            fontSize,
            top
        } // 风险分数样式
        return (
            <div className={styles.main} id="container">
                <div className={styles.colorLine} style={bgcStyle} />
                <div className={styles.list} id="list" style={bgcStyle} />
                <p className={styles.num} style={gradeStyle}>
                    {grade}
                </p>
            </div>
        )
    }
}

export default UserDefineProgressBar
