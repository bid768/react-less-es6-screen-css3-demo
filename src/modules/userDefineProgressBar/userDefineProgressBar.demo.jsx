import React, { Component } from 'react'
import UserDefineProgressBar from './userDefineProgressBar'

class UserDefineProgressBarDemo extends Component {
    render() {
        // 风险分数
        const grade = 77
        // 进度条宽度
        const progressWidth = 250
        // 进度条高度
        const progressHeight = 21
        // 字体大小
        const size = 25
        // 数字位置
        const fontTop = 10
        // visible div 宽度
        const xWidth = 10
        // hidden div 宽度
        const yWidth = 5
        // 计算分数显示相应的颜色块
        const num = 6
        return (
            <UserDefineProgressBar
                grade={grade}
                progressWidth={progressWidth}
                xWidth={xWidth}
                yWidth={yWidth}
                progressHeight={progressHeight}
                num={num}
                size={size}
                fontTop={fontTop}
            />
        )
    }
}
export default UserDefineProgressBarDemo
