import React from 'react'

class Progress extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            pValue: 0
        }
    }

    componentDidMount() {
        const _this = this
        setTimeout(function myTimer() {
            _this.setState((prevState, props) => ({
                pValue: prevState.pValue + 1
            }))
            if (_this.state.pValue === 30) {
                return
            }
            setTimeout(myTimer, 500)
        }, 500)
    }

    render() {
        return (
            <progress value={this.state.pValue} max="100">
                70%
            </progress>
        )
    }
}

// 定义默认值
Progress.defaultProps = {}

export default Progress
