import React, { Component } from 'react'
import styles from './moveCar.less'

export default class MoveCar extends Component {
    render() {
        return (
            <div className={styles.carWrapper}>
                <div className={styles.car}>
                    <div className={styles.carBody}>
                        <div className={styles.adorn1} />
                        <div className={styles.adorn2} />
                        <div className={styles.adorn3} />
                    </div>
                    <div className={styles.tube} />
                    <div className={styles.wheel + ' ' + styles.leftWheel}>
                        <div className={styles.line1} />
                        <div className={styles.line2} />
                        <div className={styles.line3} />
                        <div className={styles.line4} />
                        <div className={styles.line5} />
                    </div>
                    <div className={styles.wheel + ' ' + styles.rightWheel}>
                        <div className={styles.line1} />
                        <div className={styles.line2} />
                        <div className={styles.line3} />
                        <div className={styles.line4} />
                        <div className={styles.line5} />
                    </div>
                    <div className={styles.carHead}>
                        <div className={styles.leftWind} />
                        <div className={styles.rightWind} />
                    </div>
                    <div className={styles.ball1} />
                    <div className={styles.ball2} />
                    <div className={styles.ball3} />
                    <div className={styles.weiqi1} />
                    <div className={styles.weiqi2} />
                </div>
                <div className={styles.streetWrap}>
                    <div className={styles.street + ' ' + styles.street1}>
                        <div className={styles.stone}>
                            <div className={styles.stone1} />
                            <div className={styles.stone2} />
                            <div className={styles.stone3} />
                            <div className={styles.stone4} />
                        </div>
                    </div>
                    <div className={styles.street + ' ' + styles.street2}>
                        <div className={styles.stone}>
                            <div className={styles.stone1} />
                            <div className={styles.stone2} />
                            <div className={styles.stone3} />
                            <div className={styles.stone4} />
                        </div>
                    </div>
                </div>
                <div className={styles.cloudWrap}>
                    <div className={styles.cloud1} />
                    <div className={styles.cloud2} />
                    <div className={styles.cloud3} />
                    <div className={styles.cloud4} />
                </div>
            </div>
        )
    }
}
