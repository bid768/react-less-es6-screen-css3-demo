import React from 'react'

import styles from './cssSlide.less'

export default class CssSlide extends React.Component {
    render() {
        return (
            <div className={styles.cssSlide}>
                <ul>
                    <li>111111111</li>
                    <li>222222222</li>
                    <li>3333333333</li>
                    <li>444444444</li>
                    <li>55555555</li>
                    <li>666666666</li>
                    <li>7777777777</li>

                    <li>111111111</li>
                    <li>222222222</li>
                    <li>3333333333</li>
                    <li>444444444</li>
                    <li>55555555</li>
                    <li>66666666</li>
                    <li>7777777777</li>
                </ul>
            </div>
        )
    }
}
