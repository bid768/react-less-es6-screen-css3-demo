import React from 'react'
import SmokeIndexItem from './smokeIndexItem/smokeIndexItem.jsx'
import styles from './smokeIndex.less'

class SmokeIndex extends React.Component {
    render() {
        const number = this.props.data.length
        const { blockName, data } = this.props

        return (
            <div className={styles.smokeBox}>
                <div className={styles.tit}>
                    {blockName}&nbsp;
                    <span className={styles.num}>{number}</span>
                </div>
                <div className={styles.itemBox}>{this.renderDom(data)}</div>
            </div>
        )
    }

    renderDom(data) {
        if (data.length) {
            const resultArr = data.map((item, index) => {
                return (
                    <SmokeIndexItem
                        className="smokeIndexItem"
                        key={index}
                        data={item}
                    ></SmokeIndexItem>
                )
            })
            return resultArr
        }
    }
}

SmokeIndex.defaultProps = {
    blockName: '课程项目',
    data: [
        {
            title: '数学',
            number: 100
        },
        {
            title: '语文',
            number: 75
        },
        {
            title: '英语',
            number: 100
        },
        {
            title: '化学',
            number: 30
        },
        {
            title: '物理',
            number: 80
        },
        {
            title: '计算机',
            number: 80
        },
        {
            title: '生物',
            number: 80
        }
    ]
}

export default SmokeIndex
