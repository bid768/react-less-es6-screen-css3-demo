import React from 'react'
import styles from './smokeIndexItem.less'

class SmokeIndexItem extends React.Component {
    render() {
        const { title, number } = this.props.data
        const className = this.props.className

        let dotColor
        const percent = number / 100
        const graLineWidth = percent * 838

        if (percent <= 0.33) {
            dotColor = '#edbb06'
        } else if (percent <= 0.66) {
            dotColor = '#d9a20d'
        } else if (percent <= 1) {
            dotColor = '#db0909'
        }

        return (
            <div className={styles.eachRow + ' ' + className}>
                <div className={styles.tit}>{title}</div>
                <div className={styles.graBox}>
                    <div className={styles.start}>|</div>
                    <div
                        className={styles.gra}
                        style={{ width: graLineWidth + 'px' }}
                    ></div>
                    <div
                        className={styles.dot}
                        style={{ backgroundColor: dotColor }}
                    ></div>
                </div>
                <div className={styles.num}>{number}</div>
            </div>
        )
    }
}

SmokeIndexItem.defaultProps = {
    className: '',
    data: {
        title: '特征命中率',
        number: 100
    }
}

export default SmokeIndexItem
