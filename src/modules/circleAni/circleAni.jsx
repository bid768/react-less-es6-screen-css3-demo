import React from 'react'
import styles from './circleAni.less'
import ImgSrc from 'assets/img/circle.png'
class CircleAni extends React.Component {
    render() {
        return (
            <div className={styles.circleAniBox}>
                <div className={styles.circleAni + ' ' + styles.circleAni1}>
                    <img src={ImgSrc}></img>
                </div>
                <div className={styles.circleAni + ' ' + styles.circleAni2}>
                    <img src={ImgSrc}></img>
                </div>
                <div className={styles.circleAni + ' ' + styles.circleAni3}>
                    <img src={ImgSrc}></img>
                </div>
            </div>
        )
    }
}

// 定义默认值
CircleAni.defaultProps = {}

export default CircleAni
