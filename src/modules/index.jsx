import React from 'react'
import { NavLink, Route, Switch } from 'react-router-dom'
import styles from './index.less'
import LabelList from './labelList/labelList.jsx'
import CircleAni from './circleAni/circleAni.jsx'
import Progress from './progress/progress.jsx'
import SmokeIndex from './smokeIndex/smokeIndex.jsx'
import CssSlide from './cssSlide/cssSlide'
import ToolsBars from './toolsBars/toolsBars.demo'
import UserDefineProgressBar from './userDefineProgressBar/userDefineProgressBar.demo'
import UserDefineTagCloud from './userDefineTagCloud/userDefineTagCloud.demo'
import MoveCar from './moveCar/moveCar'
import MyClip from './myClip/myClip'
import Wanzi from './wanzi/wanzi'

export default class HomePage extends React.Component {
    render() {
        return (
            <div className={styles.container}>
                <h1>react组件库</h1>
                <div className={styles.navContent}>
                    <div className={styles.navBox}>
                        <NavLink
                            to="/list/labelList"
                            activeClassName={styles.linkActive}
                        >
                            标签自适应
                        </NavLink>
                        <NavLink
                            to="/list/circleAni"
                            activeClassName={styles.linkActive}
                        >
                            圆圈涟漪
                        </NavLink>
                        <NavLink
                            to="/list/progress"
                            activeClassName={styles.linkActive}
                        >
                            进度条
                        </NavLink>
                        <NavLink
                            to="/list/smoke"
                            activeClassName={styles.linkActive}
                        >
                            冒烟条
                        </NavLink>
                        <NavLink
                            to="/list/lunbo"
                            activeClassName={styles.linkActive}
                        >
                            轮播
                        </NavLink>
                        <NavLink
                            to="/list/toolbars"
                            activeClassName={styles.linkActive}
                        >
                            工具条
                        </NavLink>
                        <NavLink
                            to="/list/jbProgress"
                            activeClassName={styles.linkActive}
                        >
                            渐变进度条
                        </NavLink>
                        <NavLink
                            to="/list/labelcloud"
                            activeClassName={styles.linkActive}
                        >
                            标签云
                        </NavLink>
                        <NavLink
                            to="/list/movecar"
                            activeClassName={styles.linkActive}
                        >
                            小车
                        </NavLink>
                        <NavLink
                            to="/list/myclip"
                            activeClassName={styles.linkActive}
                        >
                            css裁剪
                        </NavLink>
                        <NavLink
                            to="/list/wanzi"
                            activeClassName={styles.linkActive}
                        >
                            小丸子
                        </NavLink>
                    </div>
                    <div className={styles.contentBox}>
                        <Switch>
                            <Route
                                path="/list/labelList"
                                component={LabelList}
                            />
                            <Route
                                path="/list/circleAni"
                                component={CircleAni}
                            />
                            <Route path="/list/progress" component={Progress} />
                            <Route path="/list/smoke" component={SmokeIndex} />
                            <Route path="/list/lunbo" component={CssSlide} />
                            <Route
                                path="/list/toolbars"
                                component={ToolsBars}
                            />
                            <Route
                                path="/list/jbProgress"
                                component={UserDefineProgressBar}
                            />
                            <Route
                                path="/list/labelcloud"
                                component={UserDefineTagCloud}
                            />
                            <Route path="/list/movecar" component={MoveCar} />
                            <Route path="/list/myclip" component={MyClip} />
                            <Route path="/list/wanzi" component={Wanzi} />
                        </Switch>
                    </div>
                </div>
            </div>
        )
    }
}
