import React from 'react'
import ReactDOM from 'react-dom'
import { Router, Redirect, Route, Switch } from 'react-router-dom'

// import moment from 'moment';
// import 'moment/locale/zh-cn';

import HomePage from 'modules/index'
import 'styles/base.css'
const createHistory = require('history').createBrowserHistory

class App extends React.Component {
    render() {
        return (
            <Router history={createHistory()}>
                <Switch>
                    <Route
                        path="/"
                        exact={true}
                        render={() => <Redirect to="/list" />}
                    />
                    <Route path="/list" component={HomePage} />
                </Switch>
            </Router>
        )
    }
}

ReactDOM.render(<App />, document.getElementById('root'))
