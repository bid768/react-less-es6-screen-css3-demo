# reactComponents

这是react组件库

有如下组件：

* 标签自适应

用flexbox实现。

* 圆圈涟漪动画

用css3动画实现，创建3个关键帧，把每个关键帧加到img标签上。

* 进度条动画

用html5标签实现。

* 分块渐变进度条

* 轮播

* 标签云

* 工具条

是否高亮，是否可以瞬间点击，是否切换

* 完整渐变条，百分数

* 纯css打造－爱笑的小丸子

* 纯css打造小车跑动

## 三、运行

1、安装依赖

````
yarn install
````

2、启动服务端

````
npm run server
````

3、启动客户端

````
npm run dev
````


babel-plugin-import实现antd的按需加载